#include <iostream>
#include <vector>
#include <algorithm>
#include <string.h>
#include <stdexcept>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <memory>
#include <deque>

using namespace std;

typedef unsigned char byte;

struct ROMMappedFile {
    const byte *map_begin;
    const byte *map_end;
    int fd;

    ROMMappedFile(const char *name) {
        fd = open(name, O_RDONLY);
        if(fd == -1) {
            int en = errno;
            std::string sz = "Couldn't open ";
            sz += name;
            throw std::system_error(en, std::generic_category(), sz.c_str());
        }
        struct stat st;
        if(fstat(fd, &st)==-1) {
            int en = errno;
            close(fd);
            throw std::system_error(en, std::generic_category(), "fstat failed");
        }
        map_begin = (const byte *)mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
        if(map_begin == NULL) {
            int en = errno;
            close(fd);
            throw std::system_error(en, std::generic_category(), "mmap failed");
        }
        map_end = map_begin + st.st_size;
    }

    ~ROMMappedFile() {
        munmap((void *)map_begin, map_end-map_begin);
        close(fd);
    }

    const byte *begin() const { return map_begin; }
    const byte *end()   const { return map_end;   }
};

struct DigraphIndex {
    std::deque<uint64_t> digraphs[256][256];
    uint64_t bins[64] = {0};

    void addWord(uint64_t idx, const byte *begin, const byte *end) {
        if(begin == end || begin+1 == end) return;
        for(const byte *ch = begin; ch+1!=end; ++ch) {
            auto &ref = digraphs[ch[0]][ch[1]];
            if(!ref.empty()) {
                uint64_t delta = idx - ref.back();
                int bin = 0;
                while(delta) {
                    delta >>= 1;
                    bin++;
                }
                bins[bin]++;
            }
            if(ref.empty() || ref.back() != idx) ref.push_back(idx);
        }
    }

    std::vector<uint64_t> lookup(const byte *begin, const byte *end) {
        std::vector<uint64_t> ret;
        if(begin == end || begin+1 == end) return ret;
        std::vector<uint64_t> tmp;
        for(const byte *ch = begin; ch+1!=end; ++ch) {
            std::deque<uint64_t> &ref = digraphs[ch[0]][ch[1]];
            if(ret.empty()) {
                ret = std::vector<uint64_t>(ref.begin(), ref.end());
                tmp.reserve(ret.size());
            } else {
                tmp.clear();
                std::set_intersection(ret.begin(), ret.end(),
                                      ref.begin(), ref.end(),
                                      std::back_inserter(tmp));
                tmp.swap(ret);
            }
            if(ret.empty()) return ret;
            tmp.clear();
        }
        return ret;
    }
};

int main(int argc, char *argv[]) {
    if(argc == 1) {
        return 1;
    }
    DigraphIndex index;
    char **file_list = NULL;
    int files_count = 0;
    if(strcmp(argv[1], "index")==0) {
        files_count = argc - 2;
        file_list = argv + 2;
        std::cout<<"Indexing...\n";
        for(int file_idx = 0; file_idx < files_count; ++file_idx) {
            const char * file_name = argv[file_idx + 2];
            std::cout<<file_name<<"\n";
            ROMMappedFile file(file_name);
            for(const byte *pos = file.begin(); pos != file.end(); ) {
                const byte *eol = pos;
                for( ; eol!=file.end() && *eol!='\n' && *eol!='\r'; ++eol);
                index.addWord((uint64_t(file_idx)<<60) | (pos-file.begin()), pos, eol);
                pos = eol;
                for( ; pos!=file.end() && (*pos=='\n' || *pos=='\r'); ++pos);
            }
        }
    }
    std::string sz;
    std::cout<<"Done!\n>";
    uint64_t tot = 0;
    for(int i=0; i<256; ++i) {
        for(int j = 0; j<256; ++j) {
            int sz = index.digraphs[i][j].size();
            tot += sz;
        }
    }
    std::cout<<"sz: "<<tot<<"\n";
    for(int i = 0; i<64; ++i) {
        printf("%2d: %16llu\n", i, (unsigned long long)index.bins[i]);
    }
    while(getline(std::cin, sz)) {
        const byte *s = (const byte *)sz.c_str();
        std::vector<uint64_t> res = index.lookup(s, s + sz.size());
        std::unique_ptr<ROMMappedFile> cur_file;
        int cur_file_n = -1;
        for(uint64_t r: res) {
            int file_n = r >> 60;
            uint64_t offset = r & (~(uint64_t(-1)<<60));
            if(file_n != cur_file_n) {
                cur_file.reset(new ROMMappedFile(file_list[file_n]));
            }
            std::string row;
            for(const byte *ptr = cur_file->begin() + offset; ptr != cur_file->end() && *ptr!='\n'; ++ptr) {
                row.push_back(*ptr);
            }
            if(row.find(sz) != std::string::npos) {
                std::cout<<row<<"\n";
            }
        }
        std::cout<<">";
    }
    return 0;
}
